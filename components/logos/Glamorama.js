import React from "react";

const SvgGlamorama = props => (
  <svg viewBox="0 0 107.18 80" {...props}>
    <defs>
      <linearGradient
        id="Glamorama_svg__g"
        x1={59.94}
        y1={33.86}
        x2={62.69}
        y2={36.31}
        xlinkHref="#Glamorama_svg__c"
      />
      <linearGradient
        id="Glamorama_svg__m"
        x1={65.79}
        y1={41.05}
        x2={56.66}
        y2={28.95}
        gradientTransform="translate(-7.89 4.96)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#262324" />
        <stop offset={0.02} stopColor="#363434" />
        <stop offset={0.18} stopColor="#959493" />
        <stop offset={0.28} stopColor="#d1d1cf" />
        <stop offset={0.33} stopColor="#e8e9e6" />
        <stop offset={0.62} stopColor="#ebebeb" />
        <stop offset={0.82} stopColor="#e8e9e6" />
        <stop offset={1} stopColor="#262324" />
      </linearGradient>
      <linearGradient
        id="Glamorama_svg__b"
        x1={65.26}
        y1={39.66}
        x2={57.16}
        y2={30.3}
        gradientTransform="translate(-7.89 4.96)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#e8e9e6" />
        <stop offset={0.03} stopColor="#e6e7e4" />
        <stop offset={0.38} stopColor="#d1d5d2" />
        <stop offset={0.62} stopColor="#cacfcc" />
        <stop offset={0.7} stopColor="#d1d5d2" />
        <stop offset={1} stopColor="#e8e9e6" />
      </linearGradient>
      <linearGradient
        id="Glamorama_svg__c"
        data-name="New Gradient Swatch 3"
        x1={64.84}
        y1={39.72}
        x2={57.52}
        y2={30.17}
        gradientTransform="translate(-7.89 4.96)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0.01} stopColor="#ebebea" />
        <stop offset={0.17} stopColor="#c2c3c2" />
        <stop offset={0.58} stopColor="#626262" />
        <stop offset={0.86} stopColor="#252626" />
        <stop offset={1} stopColor="#0d0e0f" />
      </linearGradient>
      <linearGradient
        id="Glamorama_svg__d"
        x1={63.5}
        y1={38.48}
        x2={58.39}
        y2={30.65}
        xlinkHref="#Glamorama_svg__c"
      />
      <linearGradient
        id="Glamorama_svg__e"
        x1={57.46}
        y1={37.62}
        x2={64.88}
        y2={32.45}
        gradientTransform="translate(-7.89 4.96)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#212124" />
        <stop offset={0.14} stopColor="#454648" />
        <stop offset={0.45} stopColor="#9fa3a4" />
        <stop offset={0.55} stopColor="#bec4c4" />
        <stop offset={0.58} stopColor="#a6abab" />
        <stop offset={0.65} stopColor="#7d8081" />
        <stop offset={0.72} stopColor="#5b5c5e" />
        <stop offset={0.79} stopColor="#404043" />
        <stop offset={0.86} stopColor="#2d2d2f" />
        <stop offset={0.93} stopColor="#222124" />
        <stop offset={1} stopColor="#1e1d20" />
      </linearGradient>
      <linearGradient
        id="Glamorama_svg__f"
        x1={63.97}
        y1={37.68}
        x2={58.35}
        y2={32.2}
        xlinkHref="#Glamorama_svg__a"
      />
      <linearGradient
        id="Glamorama_svg__a"
        x1={63.98}
        y1={37.15}
        x2={57.83}
        y2={32.36}
        gradientTransform="translate(-7.89 4.96)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#d7dad8" />
        <stop offset={0.16} stopColor="#dee0df" />
        <stop offset={0.37} stopColor="#cfd4d2" />
        <stop offset={0.64} stopColor="#b8bcbd" />
        <stop offset={0.8} stopColor="#262324" />
        <stop offset={0.93} stopColor="#a8abb3" />
        <stop offset={1} stopColor="#fff" />
      </linearGradient>
      <linearGradient
        id="Glamorama_svg__h"
        x1={59.45}
        y1={37.1}
        x2={65.55}
        y2={29.87}
        xlinkHref="#Glamorama_svg__e"
      />
      <linearGradient
        id="Glamorama_svg__i"
        data-name="Linear Gradient 1"
        x1={60.56}
        y1={34.32}
        x2={61.64}
        y2={35.42}
        gradientTransform="translate(-7.89 4.96)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#fff" />
        <stop offset={1} />
      </linearGradient>
      <linearGradient
        id="Glamorama_svg__j"
        x1={63.57}
        y1={37.86}
        x2={58.97}
        y2={32.27}
        gradientTransform="translate(-7.89 4.96)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#d7dad8" />
        <stop offset={0.16} stopColor="#dee0df" />
        <stop offset={0.37} stopColor="#cfd4d2" />
        <stop offset={0.55} stopColor="#b8bcbd" />
        <stop offset={0.69} stopColor="#262324" />
        <stop offset={0.84} stopColor="#a8abb3" />
        <stop offset={0.87} stopColor="#c6c8cd" />
        <stop offset={0.9} stopColor="#dfe0e3" />
        <stop offset={0.93} stopColor="#f1f1f3" />
        <stop offset={0.96} stopColor="#fcfcfc" />
        <stop offset={1} stopColor="#fff" />
      </linearGradient>
      <linearGradient
        id="Glamorama_svg__k"
        x1={64.84}
        y1={39.39}
        x2={60.56}
        y2={34.2}
        gradientTransform="translate(-7.89 4.96)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#fff" />
        <stop offset={0.15} stopColor="#c4c4c4" />
        <stop offset={0.32} stopColor="#898989" />
        <stop offset={0.49} stopColor="#585858" />
        <stop offset={0.64} stopColor="#323232" />
        <stop offset={0.78} stopColor="#161616" />
        <stop offset={0.91} stopColor="#060606" />
        <stop offset={1} />
      </linearGradient>
      <linearGradient
        id="Glamorama_svg__l"
        x1={65.63}
        y1={40.84}
        x2={56.85}
        y2={29.2}
        gradientTransform="translate(-7.89 4.96)"
        gradientUnits="userSpaceOnUse"
      >
        <stop offset={0} stopColor="#cacfcc" />
        <stop offset={0.06} stopColor="#d1d5d2" />
        <stop offset={0.33} stopColor="#e8e9e6" />
        <stop offset={0.62} stopColor="#cacfcc" />
        <stop offset={0.66} stopColor="#d1d5d2" />
        <stop offset={0.82} stopColor="#e8e9e6" />
        <stop offset={1} stopColor="#cacfcc" />
      </linearGradient>
      <style>{".Glamorama_svg__b{fill:#262324}"}</style>
    </defs>
    <g
      style={{
        isolation: "isolate"
      }}
    >
      <path
        className="Glamorama_svg__b"
        d="M73.95 46.44l.14.01M12.88 35.32v3.15h-1.76v10.08c0 4.5-1.85 6.75-5.58 6.75a6.83 6.83 0 01-4.77-1.47L2.42 51A4.25 4.25 0 005 52a2.55 2.55 0 001.91-.76c.53-.49.77-1.45.77-2.89v-1a4.52 4.52 0 01-3.24 1.9 3.7 3.7 0 01-3.15-2A8.8 8.8 0 010 42.12a8.65 8.65 0 011.34-5 4 4 0 013.29-2A3.58 3.58 0 017.7 37v-1.68zm-5.16 6.87a5.09 5.09 0 00-.58-2.62 1.75 1.75 0 00-3.12 0 5.46 5.46 0 00-.52 2.55 4.93 4.93 0 00.55 2.52 1.68 1.68 0 001.54 1 1.8 1.8 0 001.5-.91 4.45 4.45 0 00.63-2.54z"
      />
      <path
        className="Glamorama_svg__b"
        d="M12.69 47v-2.7h1.45V32.87h-1.45v-2.72h4.38v14.18h1.33V47z"
      />
      <path
        className="Glamorama_svg__b"
        d="M45.32 44.33v-4.49a10.38 10.38 0 00-.26-2.74 2.65 2.65 0 00-1-1.46 3.11 3.11 0 00-1.85-.53 5.51 5.51 0 00-3.73 1.6 3 3 0 00-2.74-1.6 5 5 0 00-3 1.13v-1h-4.41V38h1.51v6.34H27V40a11.9 11.9 0 00-.19-2.34 4.17 4.17 0 00-.6-1.34 3 3 0 00-1.31-1 5.17 5.17 0 00-2.16-.32c-2.29 0-3.76.93-4.46 2.77l2.38.54a2 2 0 011.69-.92 1.51 1.51 0 011.25.61 2.22 2.22 0 01.48 1.47v.71a5 5 0 00-2.12-.6 3.47 3.47 0 00-2.68 1.15 4 4 0 00-1.05 2.81 3.58 3.58 0 001 2.68 3.26 3.26 0 002.35 1 3.76 3.76 0 002.51-1.08V47h10.2v-2.7h-1.53v-5.2a4.13 4.13 0 012.14-.79c.81 0 1.2.64 1.2 1.94V47h4.45v-2.67H39V39.1a3.77 3.77 0 012.1-.79c.84 0 1.24.6 1.24 1.83V47h4.47v-2.67zm-21.24-.69c-.69.82-1.3 1.22-1.84 1.22a.93.93 0 01-.83-.45 1.68 1.68 0 01-.36-1.09 1.34 1.34 0 01.42-1.1 1.27 1.27 0 01.91-.39 3.16 3.16 0 011.7.85zM67.05 44.33V47h-6.46v-2.7h1.72V38h-1.62v-2.7H65v3a4.45 4.45 0 011.1-2.21 2.77 2.77 0 012.09-.94h.48v3.53a4.49 4.49 0 00-1.82.33 2.47 2.47 0 00-1.15 1.39 6 6 0 00-.47 2.53v1.44z"
      />
      <path
        className="Glamorama_svg__b"
        d="M95.56 44.33v-4.5a10.43 10.43 0 00-.25-2.73 2.76 2.76 0 00-1-1.46 3.08 3.08 0 00-1.86-.53 5.37 5.37 0 00-3.69 1.6 3.08 3.08 0 00-2.76-1.6 4.82 4.82 0 00-3 1.13v-1h-4.43V38h1.52v6.34h-2.83V40a11.51 11.51 0 00-.17-2.34 3.67 3.67 0 00-.61-1.33 2.81 2.81 0 00-1.29-1A5.25 5.25 0 0073 35c-2.3 0-3.77.93-4.46 2.77l2.37.54a2.07 2.07 0 011.69-.92 1.53 1.53 0 011.27.59 2.22 2.22 0 01.47 1.47v.71a4.94 4.94 0 00-2.11-.6 3.5 3.5 0 00-2.7 1.15 4 4 0 00-1 2.81 3.62 3.62 0 001 2.68 3.22 3.22 0 002.36 1 3.71 3.71 0 002.5-1.08V47h10.2v-2.7H83v-5.2a4 4 0 012.13-.79c.78 0 1.19.64 1.19 1.94V47h4.44v-2.7h-1.5v-5.2a3.77 3.77 0 012.1-.79c.85 0 1.25.6 1.25 1.83V47h4.49v-2.7zm-21.23-.69c-.68.82-1.28 1.22-1.83 1.22a1 1 0 01-.86-.45 1.78 1.78 0 01-.33-1.09 1.44 1.44 0 01.4-1.1 1.34 1.34 0 01.93-.39 3.14 3.14 0 011.69.85z"
      />
      <path
        className="Glamorama_svg__b"
        d="M107.18 44.33V47h-4.41v-.9a3.68 3.68 0 01-2.49 1.09 3.29 3.29 0 01-2.37-1 3.62 3.62 0 01-1-2.68 4 4 0 011-2.81 3.46 3.46 0 012.69-1.15 5 5 0 012.12.6v-.71a2.22 2.22 0 00-.48-1.47 1.51 1.51 0 00-1.27-.59 2.11 2.11 0 00-1.68.92L97 37.77c.68-1.84 2.16-2.77 4.45-2.77a5.24 5.24 0 012.19.39 3 3 0 011.3 1 3.78 3.78 0 01.6 1.34 9.93 9.93 0 01.19 2.34v4.29zm-4.41-1.65a3.17 3.17 0 00-1.69-.85 1.32 1.32 0 00-.93.39 1.37 1.37 0 00-.41 1.1 1.79 1.79 0 00.34 1.09 1 1 0 00.85.45 2.7 2.7 0 001.84-1.22z"
      />
      <path
        d="M53.33 46.53A6.58 6.58 0 1159.9 40a6.56 6.56 0 01-6.57 6.53z"
        fill="#1b1b1c"
      />
      <path
        d="M53.33 36.57A3.39 3.39 0 1056.7 40a3.4 3.4 0 00-3.37-3.43zm0 6.49A3.11 3.11 0 1156.44 40a3.11 3.11 0 01-3.11 3.06z"
        style={{
          mixBlendMode: "overlay"
        }}
        fill="url(#Glamorama_svg__a)"
        opacity={0.2}
      />
      <path
        d="M53.33 33.62A6.34 6.34 0 1059.68 40a6.33 6.33 0 00-6.35-6.38zm0 12.39a6 6 0 116-6.06 6.05 6.05 0 01-6 6.05z"
        fill="url(#Glamorama_svg__b)"
        opacity={0.36}
      />
      <path
        d="M53.33 33.92a6 6 0 106 6 6 6 0 00-6-6zm0 11.71A5.68 5.68 0 1159 40a5.68 5.68 0 01-5.67 5.63z"
        opacity={0.63}
        fill="url(#Glamorama_svg__c)"
      />
      <path
        d="M53.33 34.28A5.68 5.68 0 1059 40a5.66 5.66 0 00-5.67-5.72zm0 10.69a5 5 0 115-5 5 5 0 01-5 5.03z"
        fill="url(#Glamorama_svg__d)"
        opacity={0.8}
      />
      <path
        d="M53.33 35.17A4.79 4.79 0 1058.11 40a4.77 4.77 0 00-4.78-4.83zm0 8.88A4.09 4.09 0 1157.41 40a4.09 4.09 0 01-4.08 4.05z"
        fill="url(#Glamorama_svg__e)"
        opacity={0.36}
      />
      <path
        d="M53.33 36.12A3.84 3.84 0 1057.16 40a3.85 3.85 0 00-3.83-3.88zm0 7.49A3.66 3.66 0 1157 40a3.66 3.66 0 01-3.67 3.61z"
        opacity={0.6}
        fill="url(#Glamorama_svg__f)"
      />
      <image
        width={80}
        height={80}
        transform="translate(13)"
        xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAABQCAYAAACOEfKtAAAACXBIWXMAAAsSAAALEgHS3X78AAACS0lEQVR4Xu3bwWoTURjF8TN35ps0Nm1hiNkIiu1QkIIo0o0oPkLoVtC3UdFufJNQsLTUSosNuLcaS41BtyJTaRMhaTrXNzDggXAL57e+fHz8753lRN57yP9zkw7IvykgSQFJCkhSQJICkhSQpIAkBSQpIEkBSQpIUkCSApIUkKSAJAUkKSBJAUkKSFJAkgKSFJCkgCQFJCkgSQFJCkhSQJICkhSQpIAkBSQpICnUgO709Kz+cv114+H9B41nz1/U99vtIHcNcSnnvc+Hw+HBaDQ6NLPDJEkOAOQfO5+D2ze4hcbjcdbv9zfebO7kH9rv3e27d3BrZSUzsw0A+eGXTlA7J5MOTFPn6KsrL8p691sv293Z6g36Z00zKxKzzNKkFQGtCHgE4NekWdMS1G0OBn+y4uSktfduF+OLcXO2Nnu8vv7qZxzHx865NTODmQW1c1AvcHtrG+fnI/z43sPjp09wtVEHANy4fg1xnMBF0YQJ0xfUbZa+LOI4Wbu3uor5hYWN2lxt+dNRpxHH8bKLopb3Ht77ctKcaYpC+1Npc3unHrnoYG6ullevzPyeqVTKSprCRa7w3jcBdPPFpWAiBvUCAcBSK8ysaal1zaw0MzgXFwCCiwcE+AIB4O3evptfqGXVatVV0hRJnJTwvli6uRhUPCDQgJdJcJ/wZaOAJAUkKSBJAUkKSFJAkgKSFJCkgCQFJCkgSQFJCkhSQJICkhSQpIAkBSQpIEkBSQpIUkCSApIUkKSAJAUkKSBJAUkKSFJAkgKSFJCkgKS/mA+MV5Oq0SwAAAAASUVORK5CYII="
      />
      <path
        d="M53.33 37.42a2.53 2.53 0 010 5.06 2.53 2.53 0 010-5.06m0-.12A2.65 2.65 0 1056 40a2.62 2.62 0 00-2.63-2.65z"
        fill="#f1f2f1"
        opacity={0.15}
      />
      <path
        d="M55.6 40a2.27 2.27 0 11-2.27-2.27A2.27 2.27 0 0155.6 40z"
        style={{
          mixBlendMode: "multiply"
        }}
        fill="url(#Glamorama_svg__g)"
        opacity={0.8}
      />
      <path
        d="M55.14 40a1.82 1.82 0 11-1.81-1.8 1.79 1.79 0 011.81 1.8z"
        fill="url(#Glamorama_svg__h)"
        opacity={0.15}
      />
      <path
        d="M54.82 40a1.5 1.5 0 11-1.49-1.49A1.49 1.49 0 0154.82 40z"
        opacity={0.06}
        fill="url(#Glamorama_svg__i)"
      />
      <path
        d="M53.33 46a6 6 0 116-6.06 6.05 6.05 0 01-6 6.06z"
        style={{
          mixBlendMode: "multiply"
        }}
        strokeWidth={0.03}
        stroke="#e9ebe9"
        fill="none"
        opacity={0.8}
      />
      <path
        d="M53.33 46a6 6 0 116-6.06 6.05 6.05 0 01-6 6.06z"
        opacity={0.27}
        stroke="#f1f2f1"
        strokeWidth={0.03}
        fill="none"
      />
      <path
        d="M53.33 36.12A3.84 3.84 0 1057.16 40a3.85 3.85 0 00-3.83-3.88zm0 7.49A3.66 3.66 0 1157 40a3.66 3.66 0 01-3.67 3.61z"
        style={{
          mixBlendMode: "overlay"
        }}
        fill="url(#Glamorama_svg__j)"
      />
      <path
        d="M53.33 35.87A4.09 4.09 0 1057.41 40a4.08 4.08 0 00-4.08-4.13zm0 7.92A3.84 3.84 0 1157.16 40a3.84 3.84 0 01-3.83 3.79z"
        fill="url(#Glamorama_svg__k)"
        opacity={0.7}
      />
      <path
        d="M53.33 44.05A4.09 4.09 0 1157.41 40a4.09 4.09 0 01-4.08 4.05z"
        style={{
          mixBlendMode: "multiply"
        }}
        strokeWidth={0.05}
        stroke="#262324"
        opacity={0.7}
        fill="none"
      />
      <path
        d="M53.33 43.79A3.84 3.84 0 1157.16 40a3.84 3.84 0 01-3.83 3.79z"
        stroke="#000"
        strokeWidth={0.05}
        fill="none"
        opacity={0.8}
      />
      <path
        d="M52.44 37.68c.1.14 0 .47-.35.72s-.65.34-.78.2 0-.45.34-.72.66-.34.79-.2z"
        opacity={0.4}
        fill="#fff"
      />
      <path
        d="M54.26 40.66c.08.07 0 .16-.08.25s-.18.09-.24 0a.21.21 0 01.08-.24c.08-.04.21-.07.24-.01z"
        fill="#fff"
        opacity={0.2}
      />
      <path
        d="M52.83 38.75c.08.11 0 .29-.21.46s-.4.22-.46.13 0-.28.18-.44.41-.23.49-.15z"
        opacity={0.1}
        fill="#fff"
      />
      <path
        d="M53.33 32.39A7.57 7.57 0 1060.91 40a7.57 7.57 0 00-7.58-7.61zm0 14.14A6.58 6.58 0 1159.9 40a6.56 6.56 0 01-6.57 6.53z"
        fill="url(#Glamorama_svg__l)"
      />
      <path
        d="M53.33 32.39A7.57 7.57 0 1060.91 40a7.57 7.57 0 00-7.58-7.61zm0 14.14A6.58 6.58 0 1159.9 40a6.56 6.56 0 01-6.57 6.53z"
        fill="url(#Glamorama_svg__m)"
        stroke="#e8e9e6"
        strokeWidth={0.03}
      />
      <path
        d="M53.33 46.53A6.58 6.58 0 1159.9 40a6.56 6.56 0 01-6.57 6.53z"
        stroke="#e8e9e6"
        strokeWidth={0.03}
        fill="none"
      />
      <path
        d="M53.33 46.62A6.66 6.66 0 1160 40a6.66 6.66 0 01-6.67 6.62z"
        strokeWidth={0.08}
        stroke="#f1f2f1"
        fill="none"
        opacity={0.2}
      />
    </g>
  </svg>
);

export default SvgGlamorama;
