import React from "react";

const SvgLaTerceraPm = props => (
  <svg viewBox="0 0 185.72 30.32" {...props}>
    <defs>
      <clipPath id="LaTerceraPM_svg__a">
        <path
          className="LaTerceraPM_svg__a"
          d="M123.33 0h30.32v30.32h-30.32z"
        />
      </clipPath>
      <clipPath id="LaTerceraPM_svg__d">
        <path
          className="LaTerceraPM_svg__a"
          d="M156.58 6.71h11.51v16.92h-11.51z"
        />
      </clipPath>
      <clipPath id="LaTerceraPM_svg__e">
        <path
          className="LaTerceraPM_svg__a"
          d="M169.07 6.62h16.63v17.09h-16.63z"
        />
      </clipPath>
      <style>
        {
          ".LaTerceraPM_svg__a{fill:none}.LaTerceraPM_svg__c{fill:#002c42}.LaTerceraPM_svg__d{opacity:.5}"
        }
      </style>
    </defs>
    <path
      className="LaTerceraPM_svg__c"
      d="M23.36 9.08h5v14.6h2.67V9.08h5V6.71H23.36zM62.13 13.19c0-3.92-2.57-6.48-7.24-6.48h-4.34v17h2.67v-4h1.67a6.61 6.61 0 001.21-.09l2.9 4.06h3.18l-3.44-4.81a5.82 5.82 0 003.39-5.68m-7.33 4.15h-1.58V9.08h1.58c3.24 0 4.59 1.55 4.59 4.11s-1.32 4.15-4.59 4.15M71.91 21.48c-3.4 0-5.92-2.61-5.92-6.29s2.45-6.28 5.82-6.28a5.46 5.46 0 014.81 2.31l2.06-1.44a7.79 7.79 0 00-6.8-3.28 8.51 8.51 0 00-8.66 8.71 8.46 8.46 0 008.59 8.71 8.37 8.37 0 007.27-3.46L77 19a6.07 6.07 0 01-5.1 2.44M104.76 13.19c0-3.92-2.58-6.48-7.24-6.48h-4.33v17h2.68v-4h1.67a6.68 6.68 0 001.21-.09l2.9 4.13h3.18l-3.48-4.81a5.84 5.84 0 003.41-5.68m-7.34 4.15h-1.56V9.08h1.56c3.23 0 4.6 1.55 4.6 4.11s-1.33 4.15-4.6 4.15M112.93 6.36h-.16l-7.43 17.32H108l1-2.37h7.59l1 2.37h2.81l-7.47-17.32zM110 18.94l2.83-6.71 2.78 6.71zM80.52 23.68h11v-2.37h-8.33v-2.87h7.22v-2.31h-7.22v-7h8.38V6.71h-11zM40.45 18.44h7.21v-2.31h-7.21v-7h8.47V6.71H37.78v17h11.16v-2.4h-8.49zM15.86 6.38L9.45 21.32H2.67V6.73H0v17h11.12l1-2.38h7.58l1 2.38h2.83L16 6.38zm-2.77 12.56l2.81-6.71 2.77 6.71z"
    />
    <g clipPath="url(#LaTerceraPM_svg__d)">
      <path
        className="LaTerceraPM_svg__c"
        d="M156.58 6.71h4.25c4.67 0 7.25 2.51 7.25 6.45s-2.61 6.44-7.25 6.44h-1.62v4h-2.63zm4.14 10.56c3.34 0 4.68-1.57 4.68-4.13S164 9 160.72 9h-1.51v8.26z"
      />
    </g>
    <g clipPath="url(#LaTerceraPM_svg__e)">
      <path
        className="LaTerceraPM_svg__c"
        d="M169.07 6.62h.28l8.08 9.45 8-9.45h.28v17.09h-2.51v-8.78c0-.95.1-2.44.1-2.44a21.51 21.51 0 01-1.51 2l-4.25 5.13h-.3L173 14.53c-.65-.78-1.49-2-1.49-2s.11 1.49.11 2.44v8.78h-2.52V6.62z"
      />
    </g>
    <g clipPath="url(#LaTerceraPM_svg__a)">
      <path
        className="LaTerceraPM_svg__c"
        d="M138.49 30.32a15.16 15.16 0 1115.16-15.16 15.18 15.18 0 01-15.16 15.16m0-28.58a13.43 13.43 0 1013.43 13.42 13.43 13.43 0 00-13.43-13.42"
      />
      <path
        className="LaTerceraPM_svg__c"
        d="M138.49 30.32a15.16 15.16 0 1115.16-15.16 15.18 15.18 0 01-15.16 15.16m0-28.58a13.43 13.43 0 1013.43 13.44 13.44 13.44 0 00-13.43-13.44"
      />
      <path
        className="LaTerceraPM_svg__c"
        d="M139.35 14.09a.71.71 0 01-1.41 0V5.38a.71.71 0 111.41 0z"
      />
      <path
        className="LaTerceraPM_svg__c"
        d="M142.84 8.94a.64.64 0 01.88.12.64.64 0 01.12.88l-4.67 4.67a.63.63 0 01-.88-.12.64.64 0 01-.12-.88z"
      />
    </g>
  </svg>
);

export default SvgLaTerceraPm;
