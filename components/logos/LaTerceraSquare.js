import React from "react";

const SvgLaTerceraSquare = props => (
  <svg viewBox="0 0 49.7 57.73" {...props}>
    <defs>
      <style>
        {
          ".LaTercera-square_svg__a{fill:#90242f}.LaTercera-square_svg__b{fill:#fff}"
        }
      </style>
    </defs>
    <path className="LaTercera-square_svg__a" d="M0 0h49.7v47.71H0z" />
    <path
      className="LaTercera-square_svg__b"
      d="M5.95 8.76h4.79v26.17h13.69v4.24H5.95V8.76zM29.91 13h-9.04V8.76h22.86V13h-9.04v26.17h-4.78V13z"
    />
    <path
      className="LaTercera-square_svg__a"
      d="M11.72 51.62H9.64v-.98h5.27v.98h-2.09v6.02h-1.1v-6.02zM25.67 57.64h-1.31l-1.19-1.7a4.12 4.12 0 01-.51 0H22v1.67h-1.1v-7h1.79c1.93 0 3 1.06 3 2.67a2.42 2.42 0 01-1.4 2.34zM22.62 55c1.35 0 1.89-.64 1.89-1.71s-.55-1.7-1.89-1.7H22V55zM26.1 54.14a3.52 3.52 0 013.57-3.6 3.24 3.24 0 012.81 1.36l-.85.6a2.25 2.25 0 00-2-1 2.6 2.6 0 000 5.19 2.5 2.5 0 002.11-1l.85.59a3.43 3.43 0 01-3 1.43 3.48 3.48 0 01-3.54-3.59M43.26 57.64H42l-1.2-1.7a3.84 3.84 0 01-.5 0h-.69v1.67h-1.1v-7h1.79c1.93 0 3 1.06 3 2.67a2.42 2.42 0 01-1.4 2.34zm-3-2.62c1.35 0 1.89-.64 1.89-1.71s-.55-1.7-1.89-1.7h-.65V55zM44.6 57.64h-1.12l3.07-7.15h.07l3.08 7.15h-1.17l-.4-1H45zm2-4.73l-1.17 2.77h2.31zM37.8 56.66h-3.46v-1.18h2.97v-.96h-2.97v-2.9h3.46v-.98h-4.56v7h4.56v-.98zM16.7 55.48h2.97v-.96H16.7v-2.9h3.5v-.98h-4.6v7h4.6v-.98h-3.5v-1.18zM0 50.64h1.1v6.02h3.17v.98H0v-7z"
    />
    <path
      className="LaTercera-square_svg__a"
      d="M4.59 57.64H3.47l3.07-7.15h.07l3.08 7.15H8.52l-.4-1H5zm2-4.72L5.4 55.69h2.31z"
    />
  </svg>
);

export default SvgLaTerceraSquare;
